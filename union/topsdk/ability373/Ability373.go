package ability373

import (
	"errors"
	"gitee.com/danlansky/go-library/union/topsdk"
	"gitee.com/danlansky/go-library/union/topsdk/ability373/request"
	"gitee.com/danlansky/go-library/union/topsdk/ability373/response"
	"gitee.com/danlansky/go-library/union/topsdk/util"
	"log"
)

type Ability373 struct {
	Client *topsdk.TopClient
}

func NewAbility373(client *topsdk.TopClient) *Ability373 {
	return &Ability373{client}
}

/*
   聚划算商品搜索接口
*/
func (ability *Ability373) TaobaoJuItemsSearch(req *request.TaobaoJuItemsSearchRequest) (*response.TaobaoJuItemsSearchResponse, error) {
	if ability.Client == nil {
		return nil, errors.New("Ability373 topClient is nil")
	}
	var jsonStr, err = ability.Client.Execute("taobao.ju.items.search", req.ToMap(), req.ToFileMap())
	var respStruct = response.TaobaoJuItemsSearchResponse{}
	if err != nil {
		log.Println("taobaoJuItemsSearch error", err)
		return &respStruct, err
	}
	err = util.HandleJsonResponse(jsonStr, &respStruct)
	if respStruct.Body == "" || len(respStruct.Body) == 0 {
		respStruct.Body = jsonStr
	}
	return &respStruct, err
}
