package CategoryService

type GetCategoryTreeByIdParam struct {
	Service
	Request GetCategoryTreeByIdRequest
}

func (a GetCategoryTreeByIdParam) MethodName() string {
	return "getCategoryTreeById"
}

func (a GetCategoryTreeByIdParam) Params() interface{} {
	p := make(map[string]interface{}, 0)
	p["category_id"] = a.Request.CategoryID
	return p
}

type GetCategoryTreeByIdRequest struct {
	CategoryID int64 `json:"category_id"` // 分类id
}
type GetCategoryTreeByIdResponse struct {
	ReturnCode string            `json:"returnCode"`
	Result     []TwoCategoryInfo `json:"result"`
}
type TwoCategoryInfo struct {
	CategoryInfo
	Children []ThreeCategoryInfo `json:"children"`
}
type ThreeCategoryInfo struct {
	CategoryInfo
	Children []CategoryInfo `json:"children"`
}
type CategoryInfo struct {
	CategoryID   int64  `json:"category_id"`   // 分类id
	CategoryName string `json:"category_name"` //分类名称
	ForeignName  string `json:"foreignname"`   //分类名称外国名称，格式为JSON
	Keywords     string `json:"keywords"`      //关键词
	Description  string `json:"description"`   //描述
}
