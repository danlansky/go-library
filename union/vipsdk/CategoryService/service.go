package CategoryService

import (
	"gitee.com/danlansky/go-library/union/vipsdk"
)

type Service struct {
}

func (a Service) ServiceName() string {
	return "vipapis.category.CategoryService"
}

func (a Service) Version() string {
	return vipsdk.DefaultVersion
}

func (a Service) Token() bool {
	return false
}
