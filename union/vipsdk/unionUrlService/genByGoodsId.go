package unionUrlService

type GenByGoodsIdParam struct {
	Service
	Request GenByGoodsIdRequest
}

func (a GenByGoodsIdParam) MethodName() string {
	return "genByGoodsId"
}

func (a GenByGoodsIdParam) Params() interface{} {
	p := make(map[string]interface{}, 0)
	p["goodsIdList"] = a.Request.GoodsIdList
	p["requestId"] = a.Request.RequestId
	if a.Request.ChanTag != "" {
		p["chanTag"] = a.Request.ChanTag
	}
	if a.Request.StatParam != "" {
		p["statParam"] = a.Request.StatParam
	}
	return p
}

type GenByGoodsIdRequest struct {
	GoodsIdList            []string               `json:"goodsIdList,omitempty"`  // 商品id列表
	ChanTag                string                 `json:"chanTag"`                // 渠道标识
	RequestId              string                 `json:"requestId"`              // 请求id：UUID
	StatParam              string                 `json:"statParam"`              // 自定义渠道统计参数
	UrlGenByGoodsIdRequest UrlGenByGoodsIdRequest `json:"urlGenByGoodsIdRequest"` // 联盟链接请求参数
}

type UrlGenByGoodsIdRequest struct {
	OpenId   string `json:"openId"`   // 渠道用户在渠道侧的用户唯一标识（必传）default_open_id
	RealCall bool   `json:"realCall"` // 是否实时调用（必传），false
	Platform int64  `json:"platform"` // 平台类型,1:移动端，2：PC端，默认移动端
	AdCode   string `json:"adCode"`   // 必填，标识获取推广物料的来源，从物料输出接口获取，如
	// 当前转链的物料不是从联盟物料接口获取，则传默认值adCode(工具商接口传vendoapi，渠道商接口传unionapi，
	//如旧版本客户端无法传adcode，则传oldversion)，该参数用于优化用户推荐效果，请勿乱传
}

type UrlGenResponse struct {
	ReturnCode    string `json:"returnCode"`
	ReturnMessage string `json:"returnMessage"`
	Result        struct {
		URLInfoList []*UrlInfo `json:"urlInfoList"`
	} `json:"result"`
}

type UrlInfo struct {
	Source         string `json:"source"`
	URL            string `json:"url"`
	LongURL        string `json:"longUrl"`
	UlURL          string `json:"ulUrl"`
	DeepLinkURL    string `json:"deeplinkUrl"`
	TraFrom        string `json:"traFrom"`
	NoEvokeURL     string `json:"noEvokeUrl"`
	NoEvokeLongURL string `json:"noEvokeLongUrl"`
	VipWxURL       string `json:"vipWxUrl"`
	VipWxCode      string `json:"vipWxCode"`
	VipQuickAppURL string `json:"vipQuickAppUrl"`
}
