package unionGoodsService

type QueryByBrandSnParam struct {
	Service
	Request QueryByBrandSnRequest
}

func (a QueryByBrandSnParam) MethodName() string {
	return "queryByBrandSn"
}

func (a QueryByBrandSnParam) Params() interface{} {
	p := make(map[string]interface{}, 0)
	param := make(map[string]interface{}, 0)
	param["requestId"] = a.Request.RequestId
	param["chanTag"] = a.Request.ChanTag
	param["openId"] = a.Request.OpenId
	param["realCall"] = a.Request.RealCall
	param["brandSn"] = a.Request.BrandSn
	param["order"] = a.Request.Order
	if a.Request.FieldName != "" {
		param["fieldName"] = a.Request.FieldName
	}
	if a.Request.Page > 0 {
		param["page"] = a.Request.Page
	}
	if a.Request.FieldName != "" {
		param["pageSize"] = a.Request.PageSize
	} else {
		param["pageSize"] = 50
	}
	if a.Request.PageOffset > 0 {
		param["pageOffset"] = a.Request.PageOffset
	}
	if a.Request.GoodsCateId1 > 0 {
		param["goodsCateId1"] = a.Request.GoodsCateId1
	}
	if a.Request.GoodsCateId2 > 0 {
		param["goodsCateId2"] = a.Request.GoodsCateId2
	}
	if a.Request.GoodsCateId3 > 0 {
		param["goodsCateId3"] = a.Request.GoodsCateId3
	}
	p["request"] = param
	return p
}

type QueryByBrandSnRequest struct {
	BrandSn      string `json:"brandSn"`      //品牌SN
	FieldName    string `json:"fieldName"`    //排序字段
	Order        int64  `json:"order"`        //排序顺序：0-正序，1-逆序，默认正序
	Page         int64  `json:"page"`         //
	PageSize     int64  `json:"pageSize"`     //页面大小：默认20,最大50
	PageOffset   int64  `json:"pageOffset"`   //查询偏移
	GoodsCateId1 int64  `json:"goodsCateId1"` //
	GoodsCateId2 int64  `json:"goodsCateId2"` //
	GoodsCateId3 int64  `json:"goodsCateId3"` //
	RequestId    string `json:"requestId"`    // 请求id：UUID
	ChanTag      string `json:"chanTag"`      // chanTag=pid，即推广位标识 (必传)，: default_pid
	OpenId       string `json:"openId"`       // 渠道用户在渠道侧的用户唯一标识（必传）default_open_id
	RealCall     bool   `json:"realCall"`     //是否实时调用（必传），false
}
type QueryByBrandSnResponse struct {
	ReturnCode string            `json:"returnCode"`
	Result     GoodsInfoResponse `json:"result"`
}
