package unionGoodsService

type GetBrandSnListParam struct {
	Service
	Request GetBrandSnListRequest
}

func (a GetBrandSnListParam) MethodName() string {
	return "getBrandSnList"
}

func (a GetBrandSnListParam) Params() interface{} {
	p := make(map[string]interface{}, 0)
	p["request"] = a.Request
	return p
}

type GetBrandSnListRequest struct {
	RequestId string `json:"requestId"` // 请求id：UUID
	ChanTag   string `json:"chanTag"`   // chanTag=pid，即推广位标识 (必传)，: default_pid
	OpenId    string `json:"openId"`    // 渠道用户在渠道侧的用户唯一标识（必传）default_open_id
	RealCall  bool   `json:"realCall"`  //是否实时调用（必传），false
}
type GetBrandSnListResponse struct {
	ReturnCode string              `json:"returnCode"`
	Result     BrandSnListResponse `json:"result"`
}

type BrandSnListResponse struct {
	BrandSnList []string `json:"brandSnList"`
	Total       int64    `json:"total"`
}
