package unionGoodsService

type GoodsListParam struct {
	Service
	Request   GoodsInfoRequest
	RequestId string
}

func (a GoodsListParam) MethodName() string {
	return "goodsList"
}

func (a GoodsListParam) Params() interface{} {
	p := make(map[string]interface{}, 0)
	p["request"] = a.Request
	return p
}

type GoodsInfoRequest struct {
	RequestId   string `json:"requestId"`            // 请求id：UUID
	ChanTag     string `json:"chanTag"`              // chanTag=pid，即推广位标识 (必传)，: default_pid
	OpenId      string `json:"openId"`               // 渠道用户在渠道侧的用户唯一标识（必传）default_open_id
	RealCall    bool   `json:"realCall"`             // 是否实时调用（必传），false
	ChannelType int64  `json:"channelType"`          // 频道类型:0-超高佣，1-出单爆款; 当请求类型为频道时必传
	Page        int64  `json:"page,omitempty"`       // 页码
	PageSize    int64  `json:"pageSize,omitempty"`   // 分页大小:默认20，最大100
	JxCode      string `json:"jxCode,omitempty"`     // 精选组货码：当请求源类型为组货时必传
	VendorCode  string `json:"vendorCode,omitempty"` // 工具商code
	SourceType  int64  `json:"sourceType"`           // 请求源类型：0-频道，1-组货
	FieldName   string `json:"fieldName,omitempty"`  // 排序字段: COMMISSION-佣金，PRICE-价格,COMM_RATIO-佣金比例，DISCOUNT-折扣
	Order       int64  `json:"order"`                // 排序顺序：0-正序，1-逆序，默认正序
}

type GoodsListResponse struct {
	ReturnCode    string            `json:"returnCode"`
	ReturnMessage string            `json:"returnMessage,omitempty"`
	Result        GoodsInfoResponse `json:"result"`
}
