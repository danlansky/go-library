package unionGoodsService

type GetByGoodsIdsV2Param struct {
	Service
	Request GetByGoodsIdsV2Request
}

func (a GetByGoodsIdsV2Param) MethodName() string {
	return "getByGoodsIdsV2"
}

func (a GetByGoodsIdsV2Param) Params() interface{} {
	p := make(map[string]interface{}, 0)
	p["request"] = a.Request
	return p
}

type GetByGoodsIdsV2Request struct {
	GoodsIds                    []string `json:"goodsIds"`                    // 商品id列表
	RequestId                   string   `json:"requestId"`                   // 请求id：UUID
	ChanTag                     string   `json:"chanTag"`                     // chanTag=pid，即推广位标识 (必传)，: default_pid
	OpenId                      string   `json:"openId"`                      // 渠道用户在渠道侧的用户唯一标识（必传）default_open_id
	RealCall                    bool     `json:"realCall"`                    // 是否实时调用（必传），false
	QueryDetail                 bool     `json:"queryDetail"`                 // 是否查询详情信息：此参数设为true，将查询出商品轮播图和详情图，默认为false
	QueryStock                  bool     `json:"queryStock"`                  // 是否查询商品库存状态：默认为false
	QueryReputation             bool     `json:"queryReputation"`             // 是否查询商品评价信息：默认为false
	QueryStoreServiceCapability bool     `json:"queryStoreServiceCapability"` // 是否查询商品所属店铺服务能力信息：默认为false
	QueryPMSAct                 bool     `json:"queryPMSAct"`                 // 是否查询商品活动信息:默认为false
	QueryCpsInfo                int64    `json:"queryCpsInfo"`                // 是否返回cps链接：0-不查询，1-tra_from参数,2-小程序链接，默认为0，查询多个时按照位运算处理，例如：3表示查询tra_from参数+小程序链接
}
type GetByGoodsIdsV2Response struct {
	ReturnCode string       `json:"returnCode"`
	Result     []*GoodsInfo `json:"result"`
}
