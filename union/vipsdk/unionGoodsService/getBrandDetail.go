package unionGoodsService

type GetBrandDetailParam struct {
	Service
	Request GetBrandDetailRequest
}

func (a GetBrandDetailParam) MethodName() string {
	return "getBrandDetail"
}

func (a GetBrandDetailParam) Params() interface{} {
	p := make(map[string]interface{}, 0)
	p["request"] = a.Request
	return p
}

type GetBrandDetailRequest struct {
	RequestId   string   `json:"requestId"`   // 请求id：UUID
	BrandSn     string   `json:"brandSn"`     // 品牌Sn
	BrandSnList []string `json:"brandSnList"` // 品牌Sn码列表
}

type GetBrandDetailResponse struct {
	ReturnCode string              `json:"returnCode"`
	Result     BrandDetailResponse `json:"result"`
}
type BrandDetailResponse struct {
	BrandDetail     BrandInfo    `json:"brandDetail"`
	BrandDetailList []*BrandInfo `json:"brandDetailList"`
}
type BrandInfo struct {
	BrandSn         string `json:"brandSn"`         //品牌Sn
	CnName          string `json:"cnName"`          //品牌中文名称（与英文名称至少有一个）
	EnName          string `json:"enName"`          //英文名称（与中文名称至少有一个）
	ShowName        string `json:"showName"`        //前端展示名称
	Logo            string `json:"logo"`            //品牌LOGO图地址
	SLogon          string `json:"slogon"`          //品牌描述
	AtmosphereUrl   string `json:"atmosphereUrl"`   //品牌氛围图
	Content         string `json:"content"`         //品牌故事内容
	BrandStoryImage string `json:"brandStoryImage"` //品牌故事图片地址
}
