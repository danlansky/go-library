package logs

import "fmt"

type KafkaLogger struct{}

func (k KafkaLogger) Print(v ...interface{}) {
	zapLogger.Info(fmt.Sprint(v...))
}

func (k KafkaLogger) Printf(format string, v ...interface{}) {
	zapLogger.Info(fmt.Sprintf(format, v...))
}

func (k KafkaLogger) Println(v ...interface{}) {
	zapLogger.Info(fmt.Sprint(v...))
}
