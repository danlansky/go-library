package logs

import (
	"go.uber.org/zap"
	"testing"
)

func initLog() {
	var setting = Setting{
		RuntimeRootPath: "your_path",
		LocalIp:         "127.0.0.1",
		Project:         "mytest",
	}
	InitLog(setting)
	for i := 0; i < 1000; i++ {
		AccessLog("test-access", zap.String("key", "value"))
		LatencyLog("test-latency")
		ErrorLog("test-error")
	}
}

func BenchmarkLog(b *testing.B) {
	for i := 0; i < b.N; i++ {
		initLog()
	}
}
