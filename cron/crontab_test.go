package cron

import (
	"fmt"
	"time"
)

func ExampleJob_IsRunning() {
	s := NewCrontab()
	_ = s.AddTaskFunc("test", "* * * * *", func() { time.Sleep(2 * time.Second) })

	j := s.Scheduler
	fmt.Println(j.IsRunning())

	s.StartAsync()

	time.Sleep(time.Second)
	fmt.Println(j.IsRunning())

	time.Sleep(time.Second)
	s.Stop()

	time.Sleep(1 * time.Second)
	fmt.Println(j.IsRunning())
	// Output:
	// false
	// true
	// false
}
