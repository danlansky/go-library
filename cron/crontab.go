package cron

import (
	"github.com/go-co-op/gocron"
	"time"
)

type Scheduler = *gocron.Scheduler

type Crontab struct {
	Scheduler *gocron.Scheduler
}

// NewCrontab 初始化一个定时任务
// 不支持分布式调度，如果要支持分布式调度，建议使用redis锁配合实现
func NewCrontab() *Crontab {
	location, _ := time.LoadLocation("Asia/Shanghai")
	s := gocron.NewScheduler(location)
	s.TagsUnique()
	return &Crontab{Scheduler: s}
}

// AddTaskFunc 1.快速添加一个定时任务
func (c *Crontab) AddTaskFunc(cronName, cronExpression string, func_ interface{}, params ...interface{}) error {
	_, err := c.Scheduler.Tag(cronName).Cron(cronExpression).Do(func_, params...)
	return err
}

// AddTaskFuncFree 2.自由的添加任务
// 示例可以参考文档 ：https://github.com/go-co-op/gocron
func (c *Crontab) AddTaskFuncFree(callbacks ...func(scheduler Scheduler)) {
	for _, f := range callbacks {
		f(c.Scheduler)
	}
}

// StartAsync 异步启动 不阻塞当前进程
func (c *Crontab) StartAsync() {
	c.Scheduler.StartAsync()
}

// StartBlocking 同步启动
func (c *Crontab) StartBlocking() {
	c.Scheduler.StartBlocking()
}

// RemoveJobByTag 根据标签移除某个任务
func (c *Crontab) RemoveJobByTag(tag string) {
	_ = c.Scheduler.RemoveByTag(tag)
}

// RunJobByTag 临时运行某个任务(不阻塞)
func (c *Crontab) RunJobByTag(tag string) {
	_ = c.Scheduler.RunByTag(tag)
}

// Clear 清理所有的任务
func (c *Crontab) Clear() {
	c.Scheduler.Clear()
}

// Len 任务长度
func (c *Crontab) Len() int {
	return c.Scheduler.Len()
}

// Stop 停止任务, 等待所有任务执行完毕后才会停止
func (c *Crontab) Stop() {
	c.Scheduler.Stop()
}
