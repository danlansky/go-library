package middleware

import (
	"github.com/gin-gonic/gin"
	"strings"
)

// Mock mock中间件
// 调用过程：首先gin要使用这个中间件，会把param中的test_traffic开头的请求参数都设置到gin的ctx中。
// 然后service方法在发起对依赖服务的http请求时，可以设置相对应的mock参数，然后真正请求时，会将这个依赖服务对应的mock参数与ctx中存储的test_traffic请求参数进行比对，
// 如果ctx中存储的test_traffic请求参数包含这个依赖服务对应的mock参数，则进行url的替换，会请求至B端的yapi mock平台，获取提前配置好的mock结果。
func Mock(c *gin.Context) {
	for _, v := range c.Params {
		if strings.HasPrefix(v.Key, "test_traffic") {
			c.Set(v.Key, v.Value)
		}
	}
}
