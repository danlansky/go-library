package middleware

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

func GetRecoverMiddleware(errorResponse interface{}) func(ctx *gin.Context) {
	return func(ctx *gin.Context) {
		var r interface{}
		if r = recover(); r != nil {
			ctx.JSON(http.StatusOK, errorResponse)
			ctx.Abort()
		}
	}
}
