package result

import (
	"encoding/json"
	"gitee.com/danlansky/go-library/base/errors"
)

const (
	SuccessRespCode    = 0
	SuccessRespMessage = "success"
)

// Result 结果类型 使用map封装为了更加通用
type Result map[string]interface{}

// JSON 转换JSON
func (r *Result) JSON() (j string, err error) {
	jByte, err := json.Marshal(r)
	j = string(jByte)
	return
}

// ResultFieldName 为了兼容不同系统的不同返回格式 通过封装 ResultFieldName 实现保存字段的名称
type ResultFieldName struct {
	Code    string
	Message string
	Data    string
}

// NewResult 返回结果 数据内容字段为 response
func (r *ResultFieldName) NewResult(code int64, message string, data interface{}) Result {
	return Result{
		r.Code:    code,
		r.Message: message,
		r.Data:    data,
	}
}

// SuccessResp 成功的时候，返回内容的封装
func (r *ResultFieldName) Success(data interface{}) Result {
	return Result{
		r.Code:    SuccessRespCode,
		r.Message: SuccessRespMessage,
		r.Data:    data,
	}
}

// FailResp 失败的时候，返回内容的封装
func (r *ResultFieldName) Fail(err error) Result {
	bizError := errors.ConvertBizError(err)
	if bizError != nil {
		return Result{
			r.Code:    bizError.Code,
			r.Message: bizError.Message,
			r.Data:    bizError.Error(),
		}
	}
	return Result{
		r.Code:    errors.NotBizError,
		r.Message: errors.NotBizErrorMessage,
	}
}
