package result_test

import (
	"log"
	"testing"

	"gitee.com/danlansky/go-library/base/errors"
	"gitee.com/danlansky/go-library/base/result"
)

func TestResult(t *testing.T) {
	fieldName := &result.ResultFieldName{
		Code:    "code",
		Message: "status",
		Data:    "response",
	}

	newResultSuccess := fieldName.NewResult(0, "success", "new result success")
	successResult := fieldName.Success("result success")
	failResult := fieldName.Fail(errors.New(errors.BizErrorCodeBase, "result fail"))

	log.Println("-----------Result Testing-------------")
	log.Println(newResultSuccess.JSON())
	log.Println(successResult.JSON())
	log.Println(failResult.JSON())
	log.Println("----------------------------------------")

}
