package errors

const (
	BizErrorCodeBase = 1 << iota
	NotBizError
)

const (
	NotBizErrorMessage = "Error"
)
