package errors_test

import (
	"gitee.com/danlansky/go-library/base/errors"
	"log"
	"testing"
)

func TestBizError(t *testing.T) {
	log.Println("-----------BizError Testing-------------")
	log.Println(errors.New(errors.BizErrorCodeBase, "base biz error"))
	log.Println(errors.New(errors.NotBizError, "base biz error"))
	log.Println("----------------------------------------")
}
