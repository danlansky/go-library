package redis

import "gitee.com/danlansky/go-library/repository/redis"

var ConnectMap = make(map[string]*redis.Pool)

type Redis struct {
	*redis.Pool
}

func NewRedis(name string) *Redis {
	return &Redis{ConnectMap[name]}
}
