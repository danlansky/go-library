package initialization

import (
	"gitee.com/danlansky/go-library/logs"
	"sync"

	cache "gitee.com/danlansky/go-library/base/redis"
	"gitee.com/danlansky/go-library/repository/redis"
	"github.com/spf13/viper"
	"go.uber.org/zap"
)

const defaultRedisConfigTag = "redis"

var cacheOnce sync.Once

func initRedis() {
	cacheOnce.Do(func() {
		redisConfigMap := make(map[string]redis.Setting)
		_ = viper.UnmarshalKey(defaultRedisConfigTag, &redisConfigMap)

		for _, redisConfig := range redisConfigMap {
			conn := redis.InitRedisPool(redisConfig)
			cache.ConnectMap[redisConfig.Name] = conn
			logs.AccessLog("Redis init success!", zap.String("redis", redisConfig.Name))
		}
	})
}
