package initialization

import (
	"gitee.com/danlansky/go-library/logs"
	"sync"

	db "gitee.com/danlansky/go-library/base/mongo"
	"gitee.com/danlansky/go-library/repository/mongo"
	"github.com/spf13/viper"
	"go.uber.org/zap"
)

const defaultMongoDBConfigTag = "mongo"

var mongoOnce sync.Once

func initMongo() {
	mongoOnce.Do(func() {
		mongoConfigMap := make(map[string]mongo.Setting)
		_ = viper.UnmarshalKey(defaultMongoDBConfigTag, &mongoConfigMap)
		for _, mongoConfig := range mongoConfigMap {
			conn, err := mongo.InitMongo(mongoConfig)
			if err != nil {
				logs.ErrorLog("Mongo init err", zap.String("mongo", err.Error()))
				panic(err)
			}
			db.ConnectMap[mongoConfig.Name] = conn
			logs.AccessLog("MongoDB init success!", zap.String("mongo", mongoConfig.Name))
		}
	})
}
