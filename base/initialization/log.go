package initialization

import (
	"gitee.com/danlansky/go-library/logs"
	"go.uber.org/zap"
)

func initLog(c *appConfig) {
	logsConf := logs.Setting{
		RuntimeRootPath:  c.Log.RootPath,
		LocalIp:          c.Log.LocalIP,
		Project:          c.Name,
		Module:           c.Module,
		MaxRetentionDays: c.Log.MaxRetentionDays,
		IsLevel:          c.Log.IsLevel,
		StdOut:           c.Log.StdOut,
	}
	logs.InitLog(logsConf)
	logs.AccessLog("Log initialized",
		zap.String("log_path", logs.GetLogFilePath(logsConf)),
		zap.String("local_ip", c.Log.LocalIP),
		zap.Int("max_retention_days", c.Log.MaxRetentionDays),
		zap.Bool("is_level", c.Log.IsLevel),
		zap.Bool("std_out", c.Log.StdOut),
	)
}
