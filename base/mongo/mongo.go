package mongo

import "gitee.com/danlansky/go-library/repository/mongo"

var ConnectMap = make(map[string]*mongo.WrapperMongo)

type MongoDB struct {
	*mongo.WrapperMongo
}

func NewMongoDB(name string) *MongoDB {
	return &MongoDB{ConnectMap[name]}
}
