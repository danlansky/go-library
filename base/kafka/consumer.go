package kafka

import (
	"gitee.com/danlansky/go-library/repository/kafka"
)

var ConsumerConnectMap = make(map[string]*kafka.GroupConsumer)

type KafkaConsumer struct {
	*kafka.GroupConsumer
}

func NewKafkaConsumer(name string) *KafkaConsumer {
	return &KafkaConsumer{ConsumerConnectMap[name]}
}
