package kafka

import (
	"gitee.com/danlansky/go-library/repository/kafka"
)

var ProducerConnectMap = make(map[string]*kafka.Producer)

type KafkaProducer struct {
	*kafka.Producer
}

func NewKafkaProducer(name string) *KafkaProducer {
	return &KafkaProducer{ProducerConnectMap[name]}
}
