package redis

import "testing"

func testCodis() {
	var setting = Setting{
		Name:   "test",
		Type:   "codis",
		ZkPath: "/codis3/codis-demo/proxy",
		ZkHost: []string{"127.0.0.1:2181"},
	}
	redis_ := InitCodis(setting)

	for i := 0; i < 1000; i++ {
		_, _ = redis_.Set("test_c", "value1")
		_, _ = redis_.Get("test_c")
	}
}

// BenchmarkCodis go test -bench='Codis$' . -benchmem
func BenchmarkCodis(b *testing.B) {
	for i := 0; i < b.N; i++ {
		testCodis()
	}
}
