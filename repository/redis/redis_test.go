package redis

import (
	"testing"
)

//-bench：regexp 执行相应的 benchmarks，例如 -bench= （基准测试）
//-cover：开启测试覆盖率
//-run ：regexp 只运行 regexp 匹配的函数，例如 -run=Array 那么就执行包含有 Array 开头的函数；
//-v ：显示测试的详细命令
// go test -v -bench=Redis
// go test -v -run=Redis
func testRedis() {
	var setting = Setting{
		Name: "test",
		Type: "redis",
		Addr: []string{"127.0.0.1:6379"},
	}
	redis_ := InitRedisPool(setting)

	for i := 0; i < 1000; i++ {
		_, _ = redis_.Set("test_c", "value1")
		_, _ = redis_.Get("test_c")
	}
}

func BenchmarkRedis(b *testing.B) {
	for i := 0; i < b.N; i++ {
		testRedis()
	}
}

func TestRedis(t *testing.T) {
	testRedis()
}
