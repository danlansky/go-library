package redis

import "testing"

func testSentinel() {
	var setting = Setting{
		Name: "sentinel_name",
		Type: "sentinel",
		Addr: []string{"127.0.0.1:6380", "127.0.0.1:6381", "127.0.0.1:6382"},
	}
	redis_ := InitRedisSentinel(setting)

	for i := 0; i < 1000; i++ {
		_, _ = redis_.Set("test_c", "value1")
		_, _ = redis_.Get("test_c")
	}
}

// go test -run=Add
func TestAdd(t *testing.T) {
	var setting = Setting{
		Name: "sentinel_name",
		Type: "sentinel",
		Addr: []string{"127.0.0.1:6380", "127.0.0.1:6381", "127.0.0.1:6382"},
	}
	redis_ := InitRedisSentinel(setting)
	key := "bf:test"
	_ = redis_.BfReserve(key, 0.01, 1000)
	_, _ = redis_.BfAdd(key, "item")
	_, _ = redis_.BfInfo(key)
	_, _ = redis_.Del(key)
}

func BenchmarkSentinel(b *testing.B) {
	for i := 0; i < b.N; i++ {
		testSentinel()
	}
}
