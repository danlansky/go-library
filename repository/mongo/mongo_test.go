package mongo

import (
	"context"
	"fmt"
	"github.com/qiniu/qmgo/field"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"testing"
)

type UserInfo struct {
	field.DefaultField `bson:",inline"`
	Name               string `bson:"name"`
	Age                int    `bson:"age"`
	Talent             string `bson:"talent"`
}

// 不带密码
func TestNoPasswordMongo(t *testing.T) {
	setting := Setting{
		Name:               "displayDocument",
		Type:               "replica_set",
		ConnectTimeoutMs:   2000,
		SocketTimeoutMS:    2000,
		ReadPreferenceMode: "primary",
		Addr:               []string{"127.0.0.1:27017"},
		Auth:               &Auth{},
	}
	_, err := InitMongo(setting)
	if err != nil {
		fmt.Println(err)
		return
	}
}

// 带密码
func TestPasswordMongo(t *testing.T) {
	setting := Setting{
		Name:               "crawl",
		Type:               "shading",
		ConnectTimeoutMs:   2000,
		SocketTimeoutMS:    2000,
		ReadPreferenceMode: "primary",
		Addr:               []string{"127.0.0.1:27017"},
		Auth: &Auth{
			AuthMechanism: "SCRAM-SHA-1",
			AuthSource:    "admin",
			Username:      "xxxx",
			Password:      "xxxx", //包含特殊字符
			PasswordSet:   true,
		},
	}

	_, err := InitMongo(setting)
	if err != nil {
		fmt.Println(err)
		return
	}
}

func mongoPoolTest() {
	setting := Setting{
		Name: "test2",
		Type: "single",
		Addr: []string{"127.0.0.1:27017"},
	}
	mongoPool, err := InitMongo(setting)
	if err != nil {
		fmt.Println(err)
		return
	}
	var res UserInfo
	objId, _ := primitive.ObjectIDFromHex("626e26ef1445fe382a501ddc")
	for i := 0; i < 1000; i++ {
		ctx := context.Background()
		cli := mongoPool.Client.Database("test_db").Collection("test_collection")
		_ = cli.Find(ctx, bson.M{"_id": objId}, NewQueryMonOpt("test_db", "test_collection")).One(&res)
		fmt.Println(res)

	}
	_ = mongoPool.Client.Close(context.Background())
}

func BenchmarkMongoPoolTest(b *testing.B) {
	for i := 0; i < b.N; i++ {
		mongoPoolTest()
	}
}
