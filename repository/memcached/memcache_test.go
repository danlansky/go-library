package memcached

import (
	"fmt"
	"testing"
)

func testMemcached() {
	var setting = Setting{
		Name: "test",
		Addr: []string{"127.0.0.1:11214"},
	}
	//utils.InitMetric()
	mem := InitMemcached(setting)
	var item Item
	var err error
	for i := 0; i < 1000; i++ {
		item = Item{
			Key:        "test_c",
			Value:      []byte("value"),
			Expiration: 100,
		}
		err = mem.Set(&item)
		if err != nil {
			fmt.Println(err)
		}

		resPoint, err := mem.Get("test_c")
		if err != nil {
			fmt.Println(err)
		} else {
			fmt.Println(string((*resPoint).Value))
		}
	}
}

func BenchmarkMemcached(b *testing.B) {
	for i := 0; i < b.N; i++ {
		testMemcached()
	}

}
