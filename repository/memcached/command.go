package memcached

import (
	"github.com/bradfitz/gomemcache/memcache"
)

type Item = memcache.Item

func (c *WrapperMemcached) Get(key string) (item *Item, err error) {
	return c.cache.Get(key)
}

// Touch 修改key的过期时间
func (c *WrapperMemcached) Touch(key string, seconds int) (err error) {
	return c.cache.Touch(key, int32(seconds))
}

// GetMulti 多线程获取key，不建议使用
func (c *WrapperMemcached) GetMulti(keys []string) (map[string]*Item, error) {
	return c.cache.GetMulti(keys)
}

func (c *WrapperMemcached) Set(item *Item) error {
	return c.cache.Set(item)
}

// Add key存在时返回err
func (c *WrapperMemcached) Add(item *Item) error {
	return c.cache.Add(item)
}

// Replace 替换已存在的值，key不存在时返回err
func (c *WrapperMemcached) Replace(item *Item) error {
	return c.cache.Replace(item)
}

// CompareAndSwap cas命令，
func (c *WrapperMemcached) CompareAndSwap(item *Item) error {
	return c.cache.CompareAndSwap(item)
}

func (c *WrapperMemcached) Delete(key string) error {
	return c.cache.Delete(key)
}

func (c *WrapperMemcached) Increment(key string, delta int) (uint64, error) {
	return c.cache.Increment(key, uint64(delta))
}

func (c *WrapperMemcached) Decrement(key string, delta int) (uint64, error) {
	return c.cache.Decrement(key, uint64(delta))
}
