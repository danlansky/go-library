package kafka

import (
	"fmt"
	"testing"
)

func testSyncProducer() {
	var setting = ProducerSetting{
		Hosts: []string{
			"192.168.40.88:9092",
		},
		Topic:         "taoke_caiji",
		ReturnError:   true,
		ReturnSuccess: true,
	}

	producer, err := NewKafkaProducer(setting)
	if err != nil {
		fmt.Println(err)
	}
	for i := 0; i < 1000; i++ {
		_, _, err = producer.SendMsgSync(setting.Topic, "data", "{\"test\":\"value\"}")
		if err != nil {
			fmt.Println(err)
		}
	}
	producer.Close()
}

//func testAsyncProducer() {
//	var setting = ProducerSetting{
//		Hosts: []string{
//			"localhost1:9092",
//			"localhost4:9092",
//			"localhost2:9092",
//			"localhost3:9092",
//		},
//		Topic:       "test",
//		ReturnError: true,
//		ErrorCallback: func(err error) {
//			fmt.Println(err)
//		},
//	}
//
//	producer, err := NewAsyncProducer(setting)
//	if err != nil {
//		fmt.Println(err)
//	}
//	for i := 0; i < 1000; i++ {
//		err = producer.SendMsg(setting.Topic, "data", "{\"test\":\"value\"}")
//		if err != nil {
//			fmt.Println(err)
//		}
//	}
//	producer.Close()
//}

func BenchmarkSyncProducer(b *testing.B) {
	for i := 0; i < b.N; i++ {
		testSyncProducer()
	}
	// BenchmarkSyncProducer-4                6         196347404 ns/op         5111434 B/op      86962 allocs/op
}

//func BenchmarkAsyncProducer(b *testing.B) {
//	for i := 0; i < b.N; i++ {
//		testAsyncProducer()
//	}
//	// BenchmarkSyncProducer-4                6         196347404 ns/op         5111434 B/op      86962 allocs/op
//}
