package kafka

import (
	"fmt"
	"github.com/Shopify/sarama"
	"testing"
)

type Con struct{}

func (c *Con) SetupHook() {
	fmt.Println("test set up")
}

func (c *Con) CleanUpHook() {
	fmt.Println("test clean up")
}

func (c *Con) HandleMsg(msg *sarama.ConsumerMessage) error {
	fmt.Println("get key", msg.Key)
	fmt.Println("get value", msg.Value)
	// write mysql...
	return nil
}

func TestNewConsumer1(t *testing.T) {

	var setting = ConsumerSetting{
		Hosts: []string{
			"localhost1:9092",
			"localhost4:9092",
			"localhost2:9092",
			"localhost3:9092",
		},
		Topic:       "test",
		GroupId:     "test_123",
		Offset:      -1,
		ReturnError: true,
		ErrorCallback: func(err error) {
			fmt.Println(err)
		},
		AutoCommit: true,
	}

	consumer, err := NewConsumer(setting)
	if err != nil {
		fmt.Println(err)
		return
	}

	var con = Con{}

	consumer.StartGroupConsume(&con)

}

func TestNewConsumer2(t *testing.T) {

	var setting = ConsumerSetting{
		Hosts: []string{
			"localhost1:9092",
			"localhost4:9092",
			"localhost2:9092",
			"localhost3:9092",
		},
		Topic:       "test",
		GroupId:     "test_123",
		ReturnError: true,
		ErrorCallback: func(err error) {
			fmt.Println(err)
		},
		AutoCommit: false,
	}

	consumer, err := NewConsumer(setting)
	if err != nil {
		fmt.Println(err)
		return
	}

	var con = Con{}

	_ = consumer.StartGroupConsume(&con)

}
