package mysqldb

import (
	"encoding/json"
	"fmt"
	"gitee.com/danlansky/go-library/logs"
	"testing"
)

type SBaiduStar struct {
	Name int `gorm:"column:name" json:"name"`
	Id   int `gorm:"column:id" json:"id"`
}

func (a SBaiduStar) TableName() string {
	return "s_baidu_star"
}

func initMysql() {
	logs.InitLog(logs.Setting{
		RuntimeRootPath: "/",
		LocalIp:         "127.0.0.1",
		Project:         "test",
	})
	var setting = MysqlSetting{
		Name: "test",
		Type: "mysql",
		Write: DbConfig{
			Host:     "127.0.0.1:3306",
			Username: "danlansky",
			Password: "danlan_sky^&*()",
		},
		Read: DbConfig{
			Host:     "127.0.0.1:3306",
			Username: "danlansky",
			Password: "danlan_sky^&*()",
		},
		Database: "ent_spider_db",
	}
	db := InitMysqlDB(setting)
	db.SetLogger(logs.MysqlLogger{EnableNormal: true})

	var appinfo SBaiduStar

	var res []byte
	for i := 0; i < 10; i++ {
		db.Engine.Model(appinfo).Where("id=?", 30).First(&appinfo)
		res, _ = json.Marshal(appinfo)
		fmt.Println(string(res))
	}
}

// BenchmarkMysql go test -bench='Mysql$' . -benchmem
func BenchmarkMysql(b *testing.B) {
	for i := 0; i < b.N; i++ {
		initMysql()
	}
}
