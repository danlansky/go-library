package utils

import "sort"

/**
* slice(string类型)元素去重
 */
func RemoveReplicaSliceString(slc []string) []string {
	result := make([]string, 0)
	tempMap := make(map[string]bool, len(slc))
	for _, e := range slc {
		if tempMap[e] == false {
			tempMap[e] = true
			result = append(result, e)
		}
	}
	return result
}

/**
* slice(int类型)元素去重
 */
func RemoveReplicaSliceInt(slc []int) []int {
	result := make([]int, 0)
	tempMap := make(map[int]bool, len(slc))
	for _, e := range slc {
		if tempMap[e] == false {
			tempMap[e] = true
			result = append(result, e)
		}
	}
	return result
}

// CompareIntSlice 比较两个int型切片是否相等， true：相等， false：不相等
func CompareIntSlice(a, b []int) bool {

	if len(a) != len(b) {
		return false
	}

	if (a == nil) != (b == nil) {
		return false
	}

	sort.Ints(a[:])
	sort.Ints(b[:])

	for key, value := range a {
		if value != b[key] {
			return false
		}
	}

	return true
}

//值是否存在
func IsSliceString(val string, slice []string) bool {
	if len(slice) == 0 {
		return false
	}
	for _, item := range slice {
		if item == val {
			return true
		}
	}
	return false
}

//值是否存在
func IsSliceInt64(val int64, slice []int64) bool {
	if len(slice) == 0 {
		return false
	}
	for _, item := range slice {
		if item == val {
			return true
		}
	}
	return false
}

func IsSliceInt(val int, slice []int) bool {
	if len(slice) == 0 {
		return false
	}
	for _, item := range slice {
		if item == val {
			return true
		}
	}
	return false
}
