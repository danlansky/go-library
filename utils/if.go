package utils

func If[K comparable](cond bool, Then K, Else K) K {
	if cond {
		return Then
	} else {
		return Else
	}
}
