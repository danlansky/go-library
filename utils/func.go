package utils

import (
	"errors"
	"gorm.io/gorm"
	"regexp"
	"strconv"
)

func IsRecordNotFoundError(err error) bool {
	return errors.Is(err, gorm.ErrRecordNotFound)
}

//获取分页数量
func PageNum(total, pageSize int) (pageNum int) {
	if total <= 0 {
		return
	}
	if pageSize <= 1 {
		return total
	}
	pageNum = total / pageSize
	if total%pageSize > 0 {
		pageNum++
	}
	return
}

//mobile verify
func VerifyMobileFormat(mobileNum string) bool {
	//非严格检测
	regular := "^(1\\d{10})$"
	reg := regexp.MustCompile(regular)
	return reg.MatchString(mobileNum)
}

//判断字符串是否为数字
func IsNum(s string) bool {
	_, err := strconv.ParseFloat(s, 64)
	return err == nil
}
