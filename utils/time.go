package utils

import (
	"strconv"
	"time"
)

//将time.time转换时间字符串
func TimeFormat(t time.Time) string {
	if t.IsZero() {
		return ""
	}
	return t.Format("2006-01-02 15:04:05")
}

//将时间字符串转time.time
func StringToTime(timeStr string) (t time.Time) {
	if timeStr == "" {
		return
	}
	t, _ = time.Parse("2006-01-02 15:04:05", timeStr)
	return
}

//将时期字符串转日期整型时间
func DateStringToTime(dateStr string) (t time.Time) {
	if dateStr == "" {
		return
	}
	loc, _ := time.LoadLocation("Local")
	t, _ = time.ParseInLocation("2006-01-02", dateStr, loc)
	return
}

// 获取int型的年份
func DateStringToIntYear(dateStr string) int {
	return DateStringToTime(dateStr).Year()
}

// 获取int型的年份
func GetIntYear() int {
	year := time.Now().Year()
	return year
}

// 获取int型的月份
func GetIntMonth() int {
	month, _ := strconv.Atoi(time.Now().Format("01"))
	return month
}

func Int64ToDateString(timeUnix int64) string {
	if timeUnix == 0 {
		return ""
	}
	formatTimeStr := time.Unix(timeUnix, 0).Format("2006-01-02")
	return formatTimeStr
}

func Int64ToTimeString(timeUnix int64) string {
	if timeUnix == 0 {
		return ""
	}
	formatTimeStr := time.Unix(timeUnix, 0).Format("2006-01-02 15:04:05")
	return formatTimeStr
}
