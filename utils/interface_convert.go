package utils

import "time"

func InterfaceToString(data interface{}) string {
	return data.(string)
}

func InterfaceToInt64(data interface{}) int64 {
	return data.(int64)
}

func InterfaceToDuration(data interface{}) time.Duration {
	return time.Duration(data.(int64))
}
