package utils

import (
	"github.com/coocood/freecache"
)

var LocalCacheInstance LocalCache

// LocalCache 使用了freecache、支持缓存淘汰、线程安全
type LocalCache struct {
	cache     *freecache.Cache
	CacheSize int
}

func InitLocalCache(maxSizeMB int) {
	size := maxSizeMB * 1000 * 1000
	localCache := freecache.NewCache(size)
	LocalCacheInstance = LocalCache{
		cache:     localCache,
		CacheSize: size,
	}
}

func (l *LocalCache) Get(key string) string {
	value, _ := l.cache.Get([]byte(key))
	return string(value)
}

func (l *LocalCache) Set(key, value string, expire int) {
	_ = l.cache.Set([]byte(key), []byte(value), expire)
}

func (l *LocalCache) Del(key string) {
	_ = l.cache.Del([]byte(key))
}
