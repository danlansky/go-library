package utils

// Set 不支持并发操作的Set
type Set[K comparable] struct {
	innerMap map[K]struct{}
}

func NewSet[K comparable](items ...K) *Set[K] {
	s := &Set[K]{
		innerMap: make(map[K]struct{}, len(items)),
	}
	s.Add(items...)
	return s
}

func (s *Set[K]) Add(member ...K) {
	for _, v := range member {
		if s.Has(v) {
			continue
		}
		s.innerMap[v] = struct{}{}
	}
}

func (s *Set[K]) Has(member K) bool {
	if _, ok := s.innerMap[member]; ok {
		return true
	}
	return false
}

func (s *Set[K]) List() []K {
	var slice []K
	for key := range s.innerMap {
		slice = append(slice, key)
	}
	return slice
}
