package utils

import (
	"github.com/shopspring/decimal"
	"strconv"
)

//分转元
func Fen2YuanString(price int64) string {
	d := decimal.New(1, 2) //分除以100得到元
	result := decimal.NewFromInt(price).DivRound(d, 2).String()
	return result
}

//分转元
func Fen2YuanInt64(price int64) int64 {
	d := decimal.New(1, 2) //分除以100得到元
	result := decimal.NewFromInt(price).DivRound(d, 2).IntPart()
	return result
}

//元转分,乘以100后，保留整数部分
func StringYuan2FenInt64(price string) int64 {
	float64Price, _ := strconv.ParseFloat(price, 64)
	int64Price := Float64Yuan2FenInt64(float64Price)
	return int64Price
}

//元转分,乘以100后，保留整数部分
func Float64Yuan2FenInt64(price float64) int64 {
	d := decimal.New(1, 2) //分转元乘以100
	df := decimal.NewFromFloat(price).Mul(d).IntPart()
	//d1 := decimal.New(1, 0) //乘完之后，保留2为小数，需要这么一个中间参数
	//如下是满足，当乘以100后，仍然有小数位，取四舍五入法后，再取整数部分
	//dff := decimal.NewFromFloat(price).Mul(d).DivRound(d1, 0).IntPart()
	return df
}
